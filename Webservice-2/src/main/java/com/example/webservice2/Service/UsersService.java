package com.example.webservice2.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.webservice2.Entity.Users;
import com.example.webservice2.Repository.UsersRepository;

@Service
public class UsersService implements UsersServiceInterface {
	
	@Autowired
	UsersRepository usersRepository;
	
	
	@Override
	public Users createUsers(Users users) {
		return usersRepository.save(users);
}

}