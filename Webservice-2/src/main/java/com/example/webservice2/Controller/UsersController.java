package com.example.webservice2.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.webservice2.Entity.Users;
import com.example.webservice2.Service.UsersService;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	
		@Autowired
		UsersService usersService;
		
		@PostMapping("/create")
		public Users createUsers(@RequestBody Users users) {
			return usersService.createUsers(users);
		}


}
