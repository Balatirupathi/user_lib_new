package com.example.webservice.lib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibUsersServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibUsersServiceApplication.class, args);
	}

}
