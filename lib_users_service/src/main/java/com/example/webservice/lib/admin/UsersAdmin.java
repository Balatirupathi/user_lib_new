package com.example.webservice.lib.admin;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.example.webservice.lib.dto.Users;

public class UsersAdmin {
	
	public Users createUsers(Users users) {
		RestTemplate template = new RestTemplate();
		RequestEntity<Users> request = null;
		try {
			request = RequestEntity				   
					 .post(new URI("http://localhost:8081/users/create"))
				     .accept(MediaType.APPLICATION_JSON)
				     .body(users);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		ResponseEntity<Users> response = template.exchange(request, Users.class);

		return response.getBody();
		
	}

}
