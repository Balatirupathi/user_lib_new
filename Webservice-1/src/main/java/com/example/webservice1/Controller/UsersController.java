package com.example.webservice1.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.webservice.lib.dto.Users;
import com.example.webservice1.Admin.UsersOperationAdmin;




@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Autowired
	UsersOperationAdmin usersAdmin;
	
	@PostMapping("/create")
	public Users createUsers(@RequestBody Users users) {
		return usersAdmin.createusers(users);
	}

}


